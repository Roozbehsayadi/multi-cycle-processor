module dataPath(
	input logic clk,
		input logic reset);

	
		
//	 logic [5:0] opcode;
	 
	 
	 logic pcWrite;
	 logic branch;
	 logic  [1:0]pcSRC;
	 //logic  [1:0] ALUOp;
	 logic [1:0] ALUSrcB;
	 logic ALUSrcA;
	 logic regWrite;
	 logic memToReg;
	 logic regDst;
	 logic IorD;
	 logic memWrite;
	 logic IRWrite ;
	 logic [31:0] A,B;
	 logic [31:0] pc;
	 logic [31:0] pc2;
	 
	 logic [31:0] immExt;
	 logic [31:0] shiftedimmExt;
	 // alu
	 logic [2:0] ALUControl; 
	 logic [31:0] SRCA;
	 logic [31:0] SRCB;
	 logic [31:0] ALUResult; 
	 logic zero;
	 logic [31:0] instr;
	 logic [5:0] A3InputWire;
	 logic [31:0] ALUout;
	 logic [31:0] data;
	 logic [31:0] wd3Input;
	 logic [31:0] rd1; 
	 logic [31:0] rd2;
	 logic [31:0] pcjump;
	  logic [31:0] adr;
	  logic [31:0] rdOutput;
	 logic [27:0] sueOut;
	 logic PCen;
	 logic [31:0] constant  = 32'b00000000000000000000000000000100;  
	 assign pcjump = {pc[31:28],sueOut};//////////////
	 
	 
	 //---
	 
ControlUnit cu(instr[31:26],instr[5:0],reset,clk,memToReg,regDst,IorD,
					pcSRC,ALUSrcB,ALUSrcA,IRWrite,memWrite,pcWrite,branch
					,regWrite,ALUControl);	 
FlipFlopEnabled #(32) pcffen(clk,reset,PCen,pc2,pc);
MUX2to1 #(32)  adrmux(pc,ALUout,IorD,adr);
idmem mem(clk,memWrite,adr,B,rdOutput);
	
FlipFlopEnabled #(32) RDffen(clk,reset,IRWrite,rdOutput,instr); 
FlipFlop #(32)  RDff(clk,reset,rdOutput,data);
MUX2to1 #(32) wd3Inputmux(ALUout,data,memToReg,wd3Input);	
MUX2to1 #(5)  A3Inputmux(instr[20:16],instr[15:11],regDst,A3InputWire);	
signExtend se(instr[15:0],immExt);								 
registerFile rf(reset,instr[25:21],instr[20:16],A3InputWire,wd3Input 
					,regWrite, clk,rd1,rd2);
FlipFlop #(32) rfoutputffA(clk,reset,rd1,A);			
FlipFlop #(32) rfoutputffB(clk,reset,rd2,B);	
		
shiftUnit su(immExt,shiftedimmExt);	 
shiftUnitEX sue(instr[25:0],sueOut );
	
MUX4to1 #(32) aluSrcBMux(B,constant,immExt,shiftedimmExt,ALUSrcB,SRCB); 	 
MUX2to1 #(32) aluSrcAMux(pc,A,ALUSrcA,SRCA);
ALU alu(ALUControl,SRCA,SRCB,ALUResult,zero);
FlipFlop #(32) aluResultFF(clk,reset,ALUResult,ALUout);
MUX4to1 #(32) aluoutmux(ALUResult,ALUout,pcjump,pcjump,pcSRC,pc2);//////////
always_comb 
	begin 
		PCen = ( zero & branch ) | pcWrite;
	end
		

endmodule 