module shiftUnitEX(
  input logic [25:0] in,
  output logic [27:0] out
  );
  
  assign out = {in, 2'b00};
  
endmodule