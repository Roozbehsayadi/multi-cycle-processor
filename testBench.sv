module testBench();
	
	
	logic clk;
	logic reset;
	top top(clk,reset);

	
	initial 
		begin 
			reset<=1;
			#10;
			reset<=0;
			
		end
	always 
		begin 
			clk<=1;
			#5;
			clk<=0;
			#5;
		end

endmodule 