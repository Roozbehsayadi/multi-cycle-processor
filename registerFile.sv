/*module registerFile(
	input logic reset,
	input logic [4:0] regRead1, regRead2, writeRegister,
	input logic [31:0] writeData,
	input logic regWrite, // write control signal
	input logic clock,
	output logic [31:0] readData1,readData2
	);
	reg [31:0] regFile [31:0];
	assign readData1 = regFile[regRead1];
	assign readData2 = regFile[regRead2];
	always_ff @(posedge clock,reset)
	begin
	
//	   regFile[0] <= 0;
//		regFile[1] <= 0;
//		regFile[2] <= 0;
//	   regFile[3] <= 0;
//	   regFile[4] <= 0;
//	   regFile[5] <= 0;
//	   regFile[6] <= 0;
//	   regFile[7] <= 0;
	   regFile[8] <= 0;
//	   regFile[9] <= 0;
//	   regFile[10] <= 0;
//	   regFile[11] <= 0;
//	   regFile[12] <= 0;
//	   regFile[13] <= 0;
//	   regFile[14] <= 0;
//	   regFile[15] <= 0;
//	   regFile[16] <= 0;
//	   regFile[17] <= 0;
//	   regFile[18] <= 0;
//		regFile[19] <= 0;
//		regFile[20] <= 0;
//		regFile[21] <= 0;
//		regFile[22] <= 0;
//		regFile[23] <= 0;
//		regFile[24] <= 0;
//		regFile[25] <= 0;
//		regFile[26] <= 0;
//		regFile[27] <= 0;
//		regFile[28] <= 0;
//		regFile[29] <= 0;
//		regFile[30] <= 0;
//		regFile[31] <= 0;
		



		
		if (regWrite)
			regFile[writeRegister] <= writeData;
			end
endmodule	

*/
module registerFile(input logic         reset,

    
	  input logic [4:0]   ra1,
                        ra2,
                        wa3,
	 input  logic [31:0] wd3,
    input logic         regWrite,
   
    input logic         clk,
    output logic [31:0] rd1,
                        rd2
);
    logic [31:0] rf [31:0];
		
    always_ff @(posedge clk,posedge reset)
		  if(reset) begin 
						rf[0]<=0;
						rf[1]<=0;
						rf[2]<=0;
						rf[3]<=0;
						rf[4]<=0;
						rf[5]<=0;
						rf[6]<=0;
						rf[7]<=0;
						rf[8]<=0;
						rf[9]<=0;
						rf[10]<=0;
						rf[11]<=0;
						rf[12]<=0;
						rf[13]<=0;
						rf[14]<=0;
						rf[15]<=0;
						rf[16]<=0;
						rf[17]<=0;
						rf[18]<=0;
						rf[19]<=0;
						rf[20]<=0;
						rf[21]<=0;
						rf[22]<=0;
						rf[23]<=0;
						rf[24]<=0;
						rf[25]<=0;
						rf[26]<=0;
						rf[27]<=0;
						rf[28]<=0;
						rf[29]<=0;
						rf[30]<=0;
						rf[31]<=0;
						end
        else if (regWrite) rf[wa3] <= wd3;

    assign rd1 = ra1 == 5'b0 ? 32'b0 : rf[ra1];
    assign rd2 = ra2 == 5'b0 ? 32'b0 : rf[ra2];
	 
endmodule