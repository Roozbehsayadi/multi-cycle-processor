module ALUDecoder(input logic [5:0] funct, 
							input logic [1:0] ALUOp,
							output logic [2:0] ALUControl);

	always_comb
	begin
		ALUControl[2] = ALUOp[0] || ( ALUOp[1] && funct[1] );
		ALUControl[1] = (!ALUOp[1]) || (!funct[2]);
		ALUControl[0] = (funct[3]||funct[0]) && ALUOp[1];
		ALUControl = (ALUOp[0] == 1 && ALUOp[1] == 1) ? 3'b000 : ALUControl;
	end

endmodule
