module ControlUnitStateMachin(
	input logic [5:0] opcode,
	input logic clock,
	input logic reset,
	output logic pcWrite,
	output logic branch,
	output logic [1:0] pcSRC,
	output logic [1:0] ALUOp,
	output logic [1:0] ALUSrcB,
	output logic ALUSrcA,
	output logic regWrite,
	output logic memToReg,
	output logic regDst,
	output logic IorD,
	output logic memWrite,
	output logic IRWrite
	);
	
	typedef enum logic [3:0] {fetch,decode,
									  execute,ALUWriteBack,
									  branchEqual,
									  AddiExecute,AddiWriteBack,
									  AndiExecute,AndiWriteBack} stateType;
	stateType state,nextState;
	always_ff @(posedge clock, posedge reset)
	if (reset)
		state <= fetch;
	else 
		state <= nextState;
		
	
	always_comb
		case(state)
		
			fetch:
					nextState=decode;
			decode:
				
//				begin
					case (opcode)
						6'b000000: nextState = execute;
						6'b000100: nextState = branchEqual;
						6'b001000: nextState = AddiExecute;
						6'b001100: nextState = AndiExecute;
						default: nextState = fetch;
					endcase
//					if (opcode == 6'b000000) 	   // opcode = R-type
//						nextState = execute;
//					 	
//					else 
//					if (opcode == 6'b000100) // opcode =BEQ
//								nextState = branchEqual;
//					else									// opcode = ADDI
//								nextState = AddiExecute;										
//				end
			execute:
					nextState = ALUWriteBack;		
			ALUWriteBack:
				
					
					nextState = fetch;
				
			branchEqual:
				
					  //??
					//pcSRC = 1'b01
					nextState = fetch;
				
			AddiExecute:
				
					
					nextState = AddiWriteBack;
				
			AddiWriteBack:
				
					
					nextState = fetch;
				
			
			AndiExecute:
			
					nextState = AndiWriteBack;
					
			
			AndiWriteBack:
			
					nextState = fetch;
					
				
			default:
				nextState = fetch;
				
		endcase

//	assign IorD =	(state == fetch) ?	1'b0 : IorD ;
//	assign ALUSrcA =	((state == fetch) ?	1'b0 :
//						 ( (state == decode) ? 1'b0 : 
//						 ( (state == execute) ? 	1'b1 : 
//						 ( (state == branch) ?  1'b1:
//						 ( (state == AddiExecute) ? 1'b1 : ALUSrcA
//							)))));
//	assign ALUSrcB =	((state == fetch) ?	2'b01 :
//						 ( (state == decode) ? 2'b11 : 
//						 ( (state == execute) ? 2'b00 : 
//						 ( (state == branch) ?  2'b00:
//						 ( (state == AddiExecute) ? 2'b10 : ALUSrcA
//							)))));
//	assign pcWrite = state == fetch ? 1'bx : pcWrite;
//	
//	assign branch = (state == branchEqual ? 1'b1 : 1'b0 ); //////////////////////// ????????????????????????????????????????????
//	
//	assign pcSRC = (state == fetch ? 1'b0 : (state == branchEqual ? 1'b1 : 1'b0 ));
//	
//	assign ALUOp = (state == fetch ? 1'b00 : (state == decode ? 2'b00 : (state == execute ? 1'b10 : (state == branchEqual ? 1'b01 : (state == AddiExecute ? 1'b00 : ALUOp )))));
//	
//	assign regWrite = (state == ALUWriteBack ? 1'b1 : (state == AddiWriteBack ? 1'b1 : 1'b0 ));
//	
//	assign memToReg = (state == ALUWriteBack ? 1'b0 : (state == AddiWriteBack ? 1'b0 : 1'b0 ));
//	
//	assign regDst = (state == ALUWriteBack ? 1'b1 : (state == AddiWriteBack ? 1'b0 : regDst ));
//	
//	assign memWrite = 1'b0;
   assign branch = state == branchEqual ? 1: 0;
   assign pcWrite = state == fetch ? 1 : 0 ;
   assign IorD = state == fetch ? 0 : IorD;
   assign memWrite = memWrite;
   assign memToReg = state == AddiWriteBack ? 0: state == ALUWriteBack ? 0: state == AndiWriteBack ? 0 : memToReg;
   assign IRWrite = state == fetch ? 1 : IRWrite;
   assign pcSRC = state == fetch ? 2'b00: state == branchEqual ? 2'b01:pcSRC;
   assign ALUOp = state == fetch ?  2'b00: state == decode ? 2'b00: state == branchEqual ? 2'b01: state == AddiExecute ? 2'b00 : state == AndiExecute ? 2'b11 : state == execute ? 2'b10:ALUOp;
   assign ALUSrcB = state == fetch ?  2'b01: state == decode ? 2'b11: state == execute ? 2'b00 :  state == branchEqual ? 2'b00 : state == AddiExecute ? 2'b10: state == AndiExecute ? 2'b10 : ALUSrcB;
   assign ALUSrcA = state == fetch ? 0: state == decode ? 0: state == AddiExecute ? 1 : state == AndiExecute ? 1 : state == branchEqual ? 1 : state == execute ? 1: ALUSrcA  ;
   assign regWrite = state == ALUWriteBack ? 1 : state == AddiWriteBack ? 1 : state == AndiWriteBack ? 1 : regWrite;
   assign regDst = state == ALUWriteBack ? 1 : state == AddiWriteBack ? 0: state == AndiWriteBack ? 0 : regDst;

	
	//assign IRWrite = (state == fetch ? 1'bx : IRWrite);
	
endmodule
