module ControlUnit (
    input logic [5:0] Opcode,
    input logic [5:0] funct,
    input logic reset,
    input logic clk, 
    output logic memToReg,
    output logic RegDst, 
    output logic IorD,
    output logic [1:0] PCSrc,
    output logic [1:0] ALUSrcB,
    output logic ALUSrcA,
    output logic IRWrite,
    output logic MemWrite,
    output logic PCWrite, 
    output logic Branch,
    output logic RegWrite, 
    output logic [2:0] ALUControl
  );

  logic [1:0] ALUOp;

  ControlUnitStateMachin controlUnitStateMachin(
    Opcode, clk, reset, PCWrite, Branch, PCSrc, ALUOp, ALUSrcB,
    ALUSrcA, RegWrite, memToReg, RegDst, IorD, MemWrite, IRWrite
  );
  
  ALUDecoder aluDecoder(
    funct, ALUOp, ALUControl
  );
  
endmodule