
module ALU(
	input logic [2:0] ALUControl,
	input logic [31:0] A,
	input logic [31:0] B,
	output logic [31:0] ALUOut,
	output logic zero
	);
	
	assign zero = (ALUOut==0); // if aluout is 0 then zero is true
	always_ff @(ALUControl,A,B)
		case(ALUControl)
			0: ALUOut <= A & B;
			1: ALUOut <= A | B;
			2: ALUOut <= A + B;
			6: ALUOut <= A - B;
			7: ALUOut <= A < B ? 1:0;
			12:ALUOut <= ~(A | B ); //nor
			default: ALUOut <=  0;
		endcase
			  

endmodule 


